在安装完RedHat Enterprise Linux系统后， 由于 redhat的yum在线更新是收费的，如果没有注册的话不能使用，如果要使用，请配置其他YUM源。
以下为详细过程。
（此过程不需卸载RedHat Enterprise Linux (RHEL) 自己的YUM程序）
~~~
cd /etc/yum.repos.d

mv rhel-source.repo rhel-source.repo.bak

wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo
~~~~

修改CentOS-Base.repo中的$releasever为6，$basearch为x86_64

~~~
vim CentOS-Base.repo
~~~

替换第 n 行开始到最后一行中每一行所有 $releasever为 6
~~~
#多按几次esc键后输入：，然后黏贴下面的字符串回车
1,$s/$releasever/6/g
~~~

替换第 n 行开始到最后一行中每一行所有 $basearch为 x86_64
~~~
#多按几次esc键后输入：，然后黏贴下面的字符串回车
1,$s/$basearch/x86_64/g
~~~

![输入图片说明](https://git.oschina.net/uploads/images/2017/0922/164352_883f0b66_602805.png "屏幕截图.png")


清理YUM缓存等文件
~~~
yum clean all
~~~

运行yum makecache生成缓存
~~~
yum makecache
~~~

适用于所有的linux，包括Redhat、SuSE、Debian等发行版，但是在debian下要安装lsb
~~~
lsb_release -a
~~~

显示的是发行版本信息
~~~
cat /etc/issue
~~~

说明正在运行的内核版本
~~~
cat /proc/version
~~~

可显示电脑以及操作系统的相关信息
~~~
uname -a
~~~
