# 获取TGT（Ticket Grangting Ticket）

TGT是CAS为用户签发的登录票据，拥有了TGT，用户就可以证明自己在CAS成功登录过。TGT封装了Cookie值以及此Cookie值对应的用户信息。用户在CAS认证成功后，CAS生成cookie（叫TGC），写入浏览器，同时生成一个TGT对象，放入自己的缓存，TGT对象的ID就是cookie的值。当HTTP再次请求到来时，如果传过来的有CAS生成的cookie，则CAS以此cookie值为key查询缓存中有无TGT ，如果有的话，则说明用户之前登录过，如果没有，则用户需要重新登录。

~~~
curl -i -X POST -d "appinvoke=true&username=jxyb001&password=bm92ZWxs&service=http://127.0.0.1:9870/sgsms/" http://sso.isc.com:8989/isc_sso/v1/tickets
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/1010/180038_d116db33_602805.png "屏幕截图.png")

# 使用TGT获取ST（Service Ticket）

ST是CAS为用户签发的访问某一service的票据。用户访问service时，service发现用户没有ST，则要求用户去CAS获取ST。用户向CAS发出获取ST的请求，如果用户的请求中包含cookie，则CAS会以此cookie值为key查询缓存中有无TGT，如果存在TGT，则用此TGT签发一个ST，返回给用户。用户凭借ST去访问service，service拿ST去CAS验证，验证通过后，允许用户访问资源。

~~~
curl -i -X POST -d "service=http://127.0.0.1:9870/sgsms/"  http://sso.isc.com:8989/isc_sso/v1/tickets/TGT-63-bUEUmWXvMFxOj7QdqqMkwPEkVD1xGuSqc9gHvdTKNWXHaMkDia-cas01.example.org
~~~

![输入图片说明](https://git.oschina.net/uploads/images/2017/1010/180120_09be3ea2_602805.png "屏幕截图.png")

# 直接使用ST访问应用资源
~~~
127.0.0.1:9870/sgsms/base/mainLayout.jsp?ticket=ST-50-I3vzY6UAIHsONriYnux4-cas01.example.org
~~~

# 参考
> https://wiki.jasig.org/display/casum/restful+api






