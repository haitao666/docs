# 环境Centos7

# 安装gcc
~~~
yum install gcc -y
~~~

# 安装redis
~~~
wget http://download.redis.io/releases/redis-4.0.8.tar.gz
tar xzf redis-4.0.8.tar.gz
cd redis-4.0.8
make
~~~

同步时钟
~~~
yum -y install chrony  
systemctl start chronyd
systemctl status chronyd
~~~

# 设置redis访问密码
~~~
vim redis.conf
~~~
![输入图片说明](https://gitee.com/uploads/images/2018/0313/164741_d2ecb284_602805.png "屏幕截图.png")

# 启动
~~~
#加上`&`号使redis以后台程序方式运行
src/redis-server redis.conf &
~~~

# 关闭
~~~
src/redis-cli shutdown
~~~

# 客户端
~~~
src/redis-cli


set foo bar
get foo
~~~