# 下载安装文件


百度云：dm7_neoky6_64.tar.gz

链接：https://pan.baidu.com/s/1hrMFZhi 密码：hq10


~~~
mkdir -p /root/dm
cd /root/dm

tar xzf dm7_neoky6_64.tar.gz
~~~
![![输入图片说明](https://gitee.com/uploads/images/2017/1218/092825_4b47cf95_602805.png "屏幕截图.png")](https://gitee.com/uploads/images/2017/1218/092821_04a17776_602805.png "屏幕截图.png")

# 安装前准备

## 加大打开文件数的限制

~~~
vi /etc/security/limits.conf
追加：

* soft nofile 1024000
* hard nofile 1024000
hive   - nofile 1024000
hive   - nproc  1024000

reboot

ulimit -n
~~~
![输入图片说明](https://gitee.com/uploads/images/2017/1219/134010_ac3e37bc_602805.png "屏幕截图.png")

# 安装

## 以root用户图形化方式登录
~~~
cd /root/dm/dm7_neoky6_64
chmod a+x *.bin
./DMInstall.bin
~~~
![1](https://gitee.com/uploads/images/2017/1218/095650_fdfc6bba_602805.png "1.png")
![2](https://gitee.com/uploads/images/2017/1218/095712_87b1e520_602805.png "2.png")
![3](https://gitee.com/uploads/images/2017/1218/095736_e87bd8f0_602805.png "3.png")
![4](https://gitee.com/uploads/images/2017/1218/095801_3ea09123_602805.png "4.png")
![5](https://gitee.com/uploads/images/2017/1218/100630_0cf7411a_602805.png "5.png")
![6](https://gitee.com/uploads/images/2017/1218/100643_bad96b44_602805.png "6.png")
![7](https://gitee.com/uploads/images/2017/1218/100714_a6298ebf_602805.png "7.png")
![8](https://gitee.com/uploads/images/2017/1218/100729_4779541d_602805.png "8.png")
![9](https://gitee.com/uploads/images/2017/1218/101055_91f6813b_602805.png "9.png")