# 达梦实时主备数据库说明

1、主库 具有数据库的正常功能（增删改查）

2、备库 具有自动同步主库的功能（只能查看）

3、主库备库可以进行切换（自动或手工方式）

# 进入部署集群菜单

![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/103709_da117f22_602805.png "屏幕截图.png")

# 选择集群类型

![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/103757_79e5a5c4_602805.png "屏幕截图.png")

# 选择安装集群的机器

![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/103842_6b89e8f2_602805.png "屏幕截图.png")

# 勾选注册服务

![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/103924_544cf2f2_602805.png "屏幕截图.png")

# 选择初始化数据库

![输入图片说明](https://images.gitee.com/uploads/images/2018/1115/185746_426027d6_602805.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/112012_2d9fe717_602805.png "屏幕截图.png")


![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/112110_cd8175aa_602805.png "屏幕截图.png")


![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/111953_f366aa1b_602805.png "屏幕截图.png")

# 不勾选需部署监视器

![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/112303_d4dc52b8_602805.png "屏幕截图.png")

# 选择达梦数据库安装文件 .bin文件

![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/112357_bebb93e4_602805.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/112418_ae5ee8d9_602805.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/112435_c4bb736f_602805.png "屏幕截图.png")

# 集群搭建完成后，打开监控（实时主备=数据守护）
![输入图片说明](https://images.gitee.com/uploads/images/2018/1119/112506_74197a9a_602805.png "屏幕截图.png")


