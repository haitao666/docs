# 创建数据库实例、用户、赋权
~~~
mysql -u root -p
create database xht;
CREATE USER 'xht'@'%' IDENTIFIED BY "ssoSSO@123123";
GRANT all ON xht.* TO xht@'%';
~~~


# 禁用外键删除表

~~~
1、set foreign_key_checks=0;
2、删除要删除的表;
3、set foreign_key_checks=1;
~~~