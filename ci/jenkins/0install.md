下载
~~~
cd /opt
wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war
~~~

安装jdk
 
_不能使用openjdk，不然后续配置jenkins的javahome时不能识别javahome_ 

安装mvn
安装git
~~~
rz
tar zxvf jdk-8u161-linux-x64.tar.gz
mv jdk-8u161-linux-x64 jdk

alternatives --install /usr/bin/java java /opt/jdk/bin/java 3
alternatives --config java
3

wget http://mirrors.hust.edu.cn/apache/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.tar.gz
tar zxvf apache-maven-3.5.2-bin.tar.gz
mv apache-maven-3.5.2-bin maven

yum install -y git.x86_64


vi /etc/profile

追加
export M2_HOME=/opt/maven
export MAVEN_OPTS='-Xms256m -Xmx512m'
export PATH=$PATH:$M2_HOME/bin
export JAVA_HOME=/opt/jdk
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:%JAVA_HOME/bin

source /etc/profile

java -version
mvn -v
git --version
~~~

同步时钟
~~~
yum -y install chrony  
systemctl start chronyd
systemctl status chronyd
systemctl restart chronyd.service
systemctl status chronyd
~~~

启动jenkins
~~~
java -jar jenkins.war
~~~

配置jenkins
~~~
http://127.0.0.1:8080
user：admin
pwd：在/root/.jenkins/secrets/initialAdminPassword文件里（其中.jenkins位置以启动war时日志里提示的为准）
~~~
![输入图片说明](https://gitee.com/uploads/images/2018/0125/112025_b67301de_602805.png "屏幕截图.png")