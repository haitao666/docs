~~~
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-scm-plugin</artifactId>
    <version>1.0</version>
    <configuration>
     <goals>install</goals>
    </configuration>
</plugin>

<scm>
  <connection>scm:svn:svn://192.168.10.53/2.src</connection>
  <developerConnection>
   scm:svn:svn://192.168.10.53/2.src
  </developerConnection>
  <url>svn://192.168.10.53/2.src</url>
</scm>

~~~

~~~
scm:svn:svn://[username[:password]@]server_name[:port]/path_to_repository 
scm:svn:svn+ssh://[username@]server_name[:port]/path_to_repository 
scm:svn:file://[hostname]/path_to_repository 
scm:svn:http://[username[:password]@]server_name[:port]/path_to_repository 
scm:svn:https://[username[:password]@]server_name[:port]/path_to_repository 

Examples 

scm:svn:file:///svn/root/module 
scm:svn:file://localhost/path_to_repository 
scm:svn:file://my_server/path_to_repository 
scm:svn:http://svn.apache.org/svn/root/module 
scm:svn:https://username@svn.apache.org/svn/root/module 
scm:svn:https://username:password@svn.apache.org/svn/root/module
~~~