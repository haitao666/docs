~~~
wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tgz
tar -zxvf Python-3.6.5.tgz
cd Python-3.6.5
mkdir /usr/local/python3.6.5
yum install make gcc gcc-c++
yum -y install zlib*
./configure --prefix=/usr/local/python3.6.5
make
make install


#备份python旧版本信息
mv /usr/bin/python /usr/bin/python2.7.5_old
#删除旧链接并重新创建链接
rm /usr/bin/python2
ln -s /usr/bin/python2.7.5_old /usr/bin/python2


#重新建立Python3.6的环境变量
ln -s /usr/local/python3.6.5/bin/python3.6  /usr/bin/python


#查询Python版本信息
python -V

~~~