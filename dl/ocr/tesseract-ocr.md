## tesseract
~~~
##安装后，将安装目录添加到环境变量path中
##cmd中进行测试
tesseract --version
##可用语言
tesseract --list-langs
##详细帮助
tesseract --help-extra
~~~

## ocr识别
~~~
tesseract a.jpg a -l chi_sim
~~~

## 安装lang
https://github.com/tesseract-ocr/tessdoc/blob/master/Data-Files.md
~~~
tessdata	均衡速度和精度
tessdata-best	高精度
tessdata-fast	高速度

## 下载和安装字库
下载tessdata中的chi_sim.traineddata和chi_sim_vert.traineddata训练集
下载的2个文件放入D:\workspace\xht\dl\ocr\Tesseract-OCR\tessdata目录

C:\Users\Administrator>tesseract --list-langs
List of available languages (4):
chi_sim
chi_sim_vert
eng
osd
~~~

## 训练字库

### 下载jTessBoxEditor
https://github.com/nguyenq/jTessBoxEditor/tags

### 运行jTessBoxEditor
双击train.bat
> 需要jdk环境变量配置（略）

### 合并待训练的图片
![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/130214_0b2d86f6_602805.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/130809_c07ac7bf_602805.png "屏幕截图.png")
### 选择合并后的输出文件
![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/130330_33ca1af9_602805.png "屏幕截图.png")

### 新建训练目录，将合并的.tif文件放入其中
![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/142908_e1af98f2_602805.png "屏幕截图.png")

### 生成.box文件
![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/143338_294aab3c_602805.png "屏幕截图.png")

### 校准.box文件
![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/131218_10d5eaf9_602805.png "屏幕截图.png")

### 生成.lstmf
~~~
tesseract.exe xx.tif xx batch.nochop makebox
~~~

![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/133139_3d59ae07_602805.png "屏幕截图.png")































