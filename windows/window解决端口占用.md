# cmd常用命令
##查看指定端口的占用情况
```
C:\>netstat -aon|findstr "9050"

  协议    本地地址                     外部地址               状态                   PID

  TCP    127.0.0.1:9050         0.0.0.0:0              LISTENING       2016
```


## 查看PID对应的进程
```
C:\>tasklist|findstr "2016"

 映像名称                       PID 会话名              会话#       内存使用
 ========================= ======== ================
  tor.exe                     2016 Console                 0     16,064 K 
```

## 结束PID对应的进程
```
taskkill /im QQMusic.exe /f
```
