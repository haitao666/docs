# 

~~~
vim /etc/yum.repos.d/rabbitmq-erlang.repo

[rabbitmq-erlang]
name=rabbitmq-erlang
baseurl=https://dl.bintray.com/rabbitmq/rpm/erlang/20/el/7
gpgcheck=1
gpgkey=https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc
repo_gpgcheck=0
enabled=1
~~~

~~~
yum makecache

yum install erlang -y

wget https://dl.bintray.com/rabbitmq/all/rabbitmq-server/3.7.3/rabbitmq-server-3.7.3-1.el7.noarch.rpm

rpm --import https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc

yum install rabbitmq-server-3.7.3-1.el7.noarch.rpm -y

/sbin/service rabbitmq-server start
/sbin/service rabbitmq-server stop
~~~


# web方式管理rabbitmq
~~~
rabbitmq-plugins enable rabbitmq_management

guest/guest访问：
http://127.0.0.1:15672/
~~~

## 新建用户
![输入图片说明](https://gitee.com/uploads/images/2018/0226/171405_64c5dfb3_602805.png "屏幕截图.png")

## 用户赋权
~~~
rabbitmqctl set_permissions -p / admin "." "." ".*"
~~~
![输入图片说明](https://gitee.com/uploads/images/2018/0226/171518_01075666_602805.png "屏幕截图.png")