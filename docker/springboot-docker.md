~~~
#开发者电脑设置环境变量（mvn deploy打包时会用到）
DOCKER_HOST=tcp://192.168.12.25:2375
~~~

springboot pom.xml
~~~
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.2.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <groupId>org.xht</groupId>
    <artifactId>springboot-docker</artifactId>
    <version>0.0.2-SNAPSHOT</version>
    <name>springboot-docker</name>
    <description>Demo project for Spring Boot</description>



    <properties>
        <java.version>1.8</java.version>
        <docker.registry>192.168.12.25:5000</docker.registry>
        <!--<docker-registry.ssl.path>D:\workspace\xht\docker\ssl-client-key\docker-registry</docker-registry.ssl.path>-->
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>dockerfile-maven-plugin</artifactId>
                <version>1.4.10</version>
                <executions>
                    <execution>
                        <id>default</id>
                        <goals>
                            <!--如果package时不想用docker打包,就注释掉这个goal-->
                            <goal>build</goal>
                            <goal>push</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <repository>${docker.registry}/${project.artifactId}</repository>
                    <contextDirectory>${project.basedir}</contextDirectory>
                    <!--<useMavenSettingsForAuth>false</useMavenSettingsForAuth>-->
                    <tag>${project.version}</tag>
                    <buildArgs>
                        <!--提供参数向Dockerfile传递-->
                        <JAR_FILE>target/${project.build.finalName}.jar</JAR_FILE>
                    </buildArgs>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
~~~

Dockerfile
~~~
FROM java:8
MAINTAINER "xuhaitao"<job_xht@163.com>
ENTRYPOINT ["/usr/bin/java", "-jar", "/app.jar"]
ARG JAR_FILE
ADD ${JAR_FILE} /app.jar
EXPOSE 8080
~~~

DockerFile文件项目位置
![输入图片说明](https://images.gitee.com/uploads/images/2019/1217/171525_874649d7_602805.png "屏幕截图.png")

发布项目到docker仓库
![输入图片说明](https://images.gitee.com/uploads/images/2019/1217/171608_aa184cc9_602805.png "屏幕截图.png")

资料参考：
https://github.com/spotify/dockerfile-maven