在已安装docker的机器上（192.168.12.25）

~~~ 
#Docker如果需要从非SSL源管理镜像，需要把非SSL的仓库地址（192.168.12.25:5000）添加信任。
vim /etc/docker/daemon.json
{
    "insecure-registries": ["192.168.12.25:5000"]
}

#重启
systemctl daemon-reload
systemctl restart docker

#安装docker registry 私服
docker run -d -p 5000:5000 --restart always registry

#测试
#访问私服：
http://192.168.12.25:5000/v2/_catalog

#拉取镜像
docker pull nginx

#标记镜像（一般是修改后或者初次提交）
docker tag nginx 192.168.12.25:5000/nginx:tag1

#提交指定镜像
docker push 192.168.12.25:5000/nginx

#再次查看私服仓库
http://192.168.12.25:5000/v2/_catalog

~~~

