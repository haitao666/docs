原文：

https://yq.aliyun.com/articles/110806

#安装 Docker

##CentOS 7 (使用yum进行安装)

### step 1: 安装必要的一些系统工具
~~~
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
~~~
### Step 2: 添加软件源信息
~~~
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
~~~
### Step 3: 更新并安装 Docker-CE
~~~
sudo yum makecache fast
sudo yum -y install docker-ce

##若container-selinux版本太低可以下载rpm进行安装升级
https://mirrors.aliyun.com/centos/7/extras/x86_64/Packages/container-selinux-2.119.2-1.911c772.el7_8.noarch.rpm
rpm -ivh container-selinux-2.119.2-1.911c772.el7_8.noarch.rpm
~~~
### Step 4: 开启Docker服务
~~~
sudo service docker start
~~~

docker 服务器开机自启动：
~~~
systemctl is-enabled docker.service  检查服务是否开机启动
systemctl enable docker.service  将服务配置成开机启动
systemctl start docker.service  启动服务

systemctl  相关其他命令：
systemctl disable docker.service 禁止开机启动
systemctl stop docker.service  停止
systemctl restart docker.service  重启

##centos6
chkconfig docker on
chkconfig docker off
~~~

## 安装指定版本的Docker-CE:
### Step 1: 查找Docker-CE的版本:
~~~
# yum list docker-ce.x86_64 --showduplicates | sort -r
#   Loading mirror speeds from cached hostfile
#   Loaded plugins: branch, fastestmirror, langpacks
#   docker-ce.x86_64            17.03.1.ce-1.el7.centos            docker-ce-stable
#   docker-ce.x86_64            17.03.1.ce-1.el7.centos            @docker-ce-stable
#   docker-ce.x86_64            17.03.0.ce-1.el7.centos            docker-ce-stable
#   Available Packages
~~~
### Step2 : 安装指定版本的Docker-CE: (VERSION 例如上面的 17.03.0.ce.1-1.el7.centos)
~~~
# sudo yum -y install docker-ce-[VERSION]
~~~