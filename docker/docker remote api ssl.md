~~~
cd /root
mkdir ssl
cd ssl
vim ssl.sh
##vim ssl.sh start

#!/bin/bash
set -e
if [ -z $1 ];then
       echo "input file location"
       exit 0
fi
VAR=$1  
mkdir -p $1
cd $1
openssl genrsa -aes256 -out ca-key.pem 4096
openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -out ca.pem
openssl genrsa -out server-key.pem 4096
openssl req -subj "/CN=$VAR" -sha256 -new -key server-key.pem -out server.csr
echo subjectAltName = DNS:$VAR,IP:127.0.0.1 > extfile.cnf
openssl x509 -req -days 365 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out server-cert.pem -extfile extfile.cnf
openssl genrsa -out key.pem 4096
openssl req -subj '/CN=client' -new -key key.pem -out client.csr
echo extendedKeyUsage = clientAuth > extfile.cnf
openssl x509 -req -days 365 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out cert.pem -extfile extfile.cnf
rm -v client.csr server.csr
chmod -v 0400 ca-key.pem key.pem server-key.pem
chmod -v 0444 ca.pem server-cert.pem cert.pem

##vim ssl.sh end

chmod 711 ssl.sh
./ssl.sh docker-registry
#输入密码test，回车后生成如下文件
~~~

![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/150539_e23e65f3_602805.png "屏幕截图.png")

~~~
vim  /lib/systemd/system/docker.service
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock --tlsverify --tlscacert=/root/ssl/docker-registry/ca.pem --tlscert=/root/ssl/docker-registry/server-cert.pem --tlskey=/root/ssl/docker-registry/server-key.pem

#重启docker
systemctl daemon-reload
systemctl restart docker.service

#测试
curl https://docker-registry:2375/info --cert ./cert.pem --key ./key.pem --cacert ./ca.pem

#导出docker-registry.pfx,在windows电脑可以双击导入证书 docker-registry.pfx
openssl pkcs12 -export -out docker-registry.pfx -inkey key.pem -in cert.pem -certfile ca.pem

#安装证书后，可以直接访问
https://docker-registry:2375/info

~~~