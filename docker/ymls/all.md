# fastDFS

```
version: '3.1'
services:
 fastdfs:
   build: environment
   restart: always
   container_name: fastdfs
   volumes:
     - ./storage:/fastdfs/storage
   network_mode: host
```

# Spring Cloud Config
```
version: '3.1'
services:
 itoken-config:
   restart: always
   image: IP地址:端口/itoken-config
   container_name: itoken-config
   ports:
     - 8888:8888
```

# Dockerfile文件内容
```
FROM openjdk:8-jre

RUN mkdir /app

COPY itoken-config-1.0.0-SNAPSHOT.jar /app/

CMD java -jar /app/itoken-config-1.0.0-SNAPSHOT.jar --spring.profiles.active=prod

EXPOSE 8888
```
# MySQL

需要在docker-compose.yml同级目录分别建立3个文件夹

>  conf 配置

>  data 数据

>  logs 日志
```
version: '3.1'
services:
 mysql:
   restart: always
   image: mysql:5.7.22
   container_name: itoken_database
   ports:
     - 3306:3306
   environment:
     TZ: Asia/Shanghai
     MYSQL_ROOT_PASSWORD: 123456
   command:
     --character-set-server=utf8mb4
     --collation-server=utf8mb4_general_ci
     --explicit_defaults_for_timestamp=true
     --lower_case_table_names=1
     --max_allowed_packet=128M
     --sql-mode="STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION,NO_ZERO_DATE,NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO"
   volumes:
     - mysql-data:/var/lib/mysql

volumes:
 mysql-data:
```
# Nginx

需要在docker-compose.yml同级目录分别建立2个文件夹

> conf 配置

> wwwroot 页面

```
version: '3.1'
services:
 nginx:
   restart: always
   image: ip:port/nginx
   container_name: nginx
   ports:
     - 81:80
     # 端口映射根据需求设置
     - 9000:9000
   volumes:
     - ./conf/nginx.conf:/etc/nginx/nginx.conf
     - ./wwwroot:/usr/share/nginx/wwwroot
```

## 以下是Nginx不带注释的配置 nginx.conf

```
worker_processes  1;

events {
   worker_connections  1024;
}

http {
   include       mime.types;
   default_type  application/octet-stream;

   sendfile        on;

   keepalive_timeout  65;
  
   server {
       listen       80;
       server_name 192.168.231.129;
       location / {
          # Nginx解决浏览器跨域问题
          add_header Access-Control-Allow-Origin *;
          add_header Access-Control-Allow-Headers X-Requested-With;
          add_header Access-Control-Allow-Methods GET,POST,PUT,DELETE,PATCH,OPTIONS;
          root   /usr/share/nginx/wwwroot/cdn;
          index  index.html index.htm;
       }

   }
}
```

## 以下是Nginx带注释的配置 nginx.conf

```
# CPU内核数量
worker_processes  1;
# 每个内核处理多少个连接
events {
   worker_connections  1024;
}

http {
   include       mime.types;
   default_type  application/octet-stream;

   sendfile        on;
   
   keepalive_timeout  65;
   # 配置虚拟主机 192.168.231.129
   server {
   # 监听的ip和端口，配置 >192.168.231.129:80
      listen       80;
   # 虚拟主机名称这里配置ip地址
   #   server_name  192.168.231.129;
      server_name  daxian.com;
   # 所有的请求都以 / 开始，所有的请求都可以匹配此 location
       location / {
      # 使用 root 指令指定虚拟主机目录即网页存放目录
      # 比如访问 http://ip/index.html 将找到 /usr/local/docker/nginx/wwwroot/html80/index.html
      # 比如访问 http://ip/item/index.html 将找到 /usr/local/docker/nginx/wwwroot/html80/item/index.html

           root   /usr/share/nginx/wwwroot/html81;
      # 指定欢迎页面，按从左到右顺序查找
           index  index.html index.htm;
       }

   }
   # 配置虚拟主机 192.168.231.129 每开一个虚拟主机就是开一个server
   server {
       listen       9000;
       server_name  192.168.231.129;

       location / {
           root   /usr/share/nginx/wwwroot/html9000;
           index  index.html index.htm;
       }
   }
}
```

## 以下是Nginx带负载均衡的配置 nginx.conf

```
# nginx负载均衡配置
user  nginx;
worker_processes  1;

events {
   worker_connections  1024;
}

http {
   include       mime.types;
   default_type  application/octet-stream;

   sendfile        on;

   keepalive_timeout  65;
  
   upstream myapp1 {
      server 192.168.231.129:9090 weight=10;
      server 192.168.231.129:9091 weight=10;
   }

   server {
      listen 80;
      server_name 192.168.231.129;
      location / {
          proxy_pass http://myapp1;
          index index.jsp index.html index.htm;
      }
   }
}
```

# Redis

```
version: '3.1'
services:
 master:
   image: redis
   restart: always
   container_name: redis-master
   ports:
     - 6379:6379

 slave1:
   image: redis
   restart: always
   container_name: redis-slave-1
   ports:
     - 6380:6379
   command: redis-server --slaveof redis-master 6379

 slave2:
   image: redis
   restart: always
   container_name: redis-slave-2
   ports:
     - 6381:6379
   command: redis-server --slaveof redis-master 6379
```

## Redis Sentinel

需要docker-compose.yml同级目录建立三个配置文件，文件名分别为sentinel1.conf、sentinel2.conf、sentinel3.conf

sentinel.conf内容为（根据这份文件复制3份conf）

```
port 26379
dir /tmp
# 自定义集群名，其中 127.0.0.1 为 redis-master 的 ip，6379 为 redis-master 的端口，2 为最小投票数（因为有 3 台 Sentinel 所以可以设置成 2）
sentinel monitor mymaster 192.168.231.129 6379 2
sentinel down-after-milliseconds mymaster 30000
sentinel parallel-syncs mymaster 1
sentinel failover-timeout mymaster 180000
sentinel deny-scripts-reconfig yes
```

docker-compose.yml内容为

```
version: '3.1'
services:
 sentinel1:
   image: redis
   container_name: redis-sentinel-1
   ports:
     - 26379:26379
   command: redis-sentinel /usr/local/etc/redis/sentinel.conf
   volumes:
     - ./sentinel1.conf:/usr/local/etc/redis/sentinel.conf

 sentinel2:
   image: redis
   container_name: redis-sentinel-2
   ports:
     - 26380:26379
   command: redis-sentinel /usr/local/etc/redis/sentinel.conf
   volumes:
     - ./sentinel2.conf:/usr/local/etc/redis/sentinel.conf

 sentinel3:
   image: redis
   container_name: redis-sentinel-3
   ports:
     - 26381:26379
   command: redis-sentinel /usr/local/etc/redis/sentinel.conf
   volumes:
     - ./sentinel3.conf:/usr/local/etc/redis/sentinel.conf
```

# RabbitMQ

需要建立spring-boot-amqp-provider消息队列提供者，spring-boot-amqp-consumer消息队列消费者两个项目，并在application.yml设置连接上RabbitMQ

```
version: '3.1'
services:
 rabbitmq:
   restart: always
   image: rabbitmq:management
   container_name: rabbitmq
   ports:
     - 5672:5672
     - 15672:15672
   environment:
     TZ: Asia/Shanghai
     RABBITMQ_DEFAULT_USER: rabbit
     RABBITMQ_DEFAULT_PASS: 123456
   volumes:
     - ./data:/var/lib/rabbitmq
```

# Nexus ( Maven私有仓库 )

需要docker-compose.yml同级目录建立一个data文件夹用于存放数据

```
version: '3.1'
services:
 nexus:
   restart: always
   image: sonatype/nexus3
   container_name: nexus
   ports:
     - 8081:8081
   volumes:
     - /usr/local/docker/nexus/data:/nexus-data
```

# Registry（ Docker私有镜像仓库 ）

需要docker-compose.yml同级目录建立两个文件夹，分别为certs和data

```
version: '3.1'
services:
 # registry服务
 registry:
   image: registry
   restart: always
   container_name: registry
   ports:
     - 5000:5000
   volumes:
     - /usr/local/docker/registry/data:/var/lib/registry
 # 前端页面
 frontend:
   image: konradkleine/docker-registry-frontend:v2
   restart: always
   ports:
     - 8080:80
   volumes:
     - ./certs/frontend.crt:/etc/apache2/server.crt:ro
     - ./certs/frontend.key:/etc/apache2/server.key:ro
   environment:
     - ENV_DOCKER_REGISTRY_HOST=192.168.231.129
     - ENV_DOCKER_REGISTRY_PORT=5000
```