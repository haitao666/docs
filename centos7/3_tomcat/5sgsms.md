# sgsms

## 准备
~~~
su - root

cd /home/oracle/tomcat

wget ftp://192.168.9.5/sgsms/centos/5sgsms.zip --ftp-user=nice --ftp-password=nice

unzip 5sgsms.zip
~~~

## 清空日志
~~~
cd /home/oracle/tomcat/5sgsms/logs
rm -rf *.log
~~~

## 启动
~~~
cd /home/oracle/tomcat/5sgsms/bin
chmod a+x *.sh

#后台启动 or
./catalina.sh start
#前台启动
./catalina.sh run
~~~

## 查看日志
~~~
tail -f /home/oracle/tomcat/5sgsms/logs/catalina.out
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/111415_51820615_602805.png "屏幕截图.png")

## 关闭
~~~
ps -ef|grep 5sgsms
kill -9 pid
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/111221_49edcdf8_602805.png "屏幕截图.png")