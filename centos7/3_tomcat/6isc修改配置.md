## 修改isc客户端应用地址

root用户以图形化方式登录系统

http://isc.mp.com:9898/isc_mp

用户名/密码：soaadmin/novell

![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/180634_b7c35c3c_602805.png "屏幕截图.png1")

![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/180741_467af5bb_602805.png "屏幕截图.png2")

![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/181015_03e31f54_602805.png "屏幕截图.png3")

![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/181053_7f1f4b67_602805.png "屏幕截图.png4")

## 登录二次运维系统

http://192.168.11.98:9870/sgsms

用户名/密码：soaadmin/novell

> soaadmin账户并未分配菜单，仅为测试登录功能，请使用其他业务账号登录系统
