# sgsms

## 准备
~~~
su - root

cd /home/oracle/tomcat

下载tomcat
百度云地址：
链接：https://pan.baidu.com/s/1sloNDl7 密码：bb9u

上传windows文件到虚拟机
rz

tar xvfz apache-tomcat-7.0.82.tar.gz

重命名
mv apache-tomcat-7.0.82 5sgsms

cd webapps

上传war包到虚拟机
rz
~~~

## 清空日志
~~~
cd /home/oracle/tomcat/5sgsms/logs
rm -rf *.log
~~~

## 启动
~~~
cd /home/oracle/tomcat/5sgsms/bin
chmod a+x *.sh

#后台启动 or
./catalina.sh start
#前台启动
./catalina.sh run
~~~

## 查看日志
~~~
tail -f /home/oracle/tomcat/5sgsms/logs/catalina.out
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/111415_51820615_602805.png "屏幕截图.png")

## 关闭
~~~
ps -ef|grep 5sgsms
kill -9 pid
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/111221_49edcdf8_602805.png "屏幕截图.png")