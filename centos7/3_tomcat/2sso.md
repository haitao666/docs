## 准备

~~~
su - root

cd /home/oracle/tomcat

wget ftp://192.168.9.5/sgsms/centos/2sso.zip --ftp-user=nice --ftp-password=nice

unzip 2sso.zip
~~~

## 清空日志
~~~
echo '' > /home/oracle/tomcat/2sso/logs/catalina.out

cd /home/oracle/tomcat/2sso/logs
rm -rf *.log

~~~

## 启动
~~~
cd /home/oracle/tomcat/2sso/bin
chmod a+x *.sh


#后台启动（推荐） or
./catalina.sh start
#前台启动(关闭命令窗口，或者ctrl+c后tomcat都会停止)
./catalina.sh run
~~~

## 查看启动的实时日志（ctrl+c后，不影响tomcat的启动状态）
~~~
tail -f /home/oracle/tomcat/2sso/logs/catalina.out
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/171227_bad7d523_602805.png "屏幕截图.png")

## 关闭2sso
~~~
ps -ef|grep 2sso
kill -9 pid
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/171002_b52985ee_602805.png "屏幕截图.png")

