## 准备
~~~
su - root

mkdir -p /home/oracle/tomcat

cd /home/oracle/tomcat

wget ftp://192.168.9.5/sgsms/centos/1mq.zip --ftp-user=nice --ftp-password=nice

unzip 1mq.zip

~~~

## 清空日志文件
~~~
cd /home/oracle/tomcat/1mq/data

echo '' > wrapper.log
echo '' > activemq.log
~~~

## 启动activemq
~~~
cd /home/oracle/tomcat/1mq/bin
chmod a+x activemq
./activemq start
~~~

## 查看启动日志
~~~
tail -f ../data/activemq.log 
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/155725_1d475a09_602805.png "屏幕截图.png")


## 查看进程
~~~
ps -ef|grep 1mq
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/162734_5b3063c1_602805.png "屏幕截图.png")


## 关闭activemq

### 方法1

![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/102952_4516a1b9_602805.png "屏幕截图.png")

### 方法2
~~~
cd /home/oracle/tomcat/1mq/bin
./activemq stop
~~~

























