# 3isc

## 准备
~~~
su - root

cd /home/oracle/tomcat

wget ftp://192.168.9.5/sgsms/centos/3isc.zip --ftp-user=nice --ftp-password=nice

unzip 3isc.zip
~~~

## 清除日志
~~~
echo '' > tail -f /home/oracle/tomcat/3isc/logs/catalina.out

cd /home/oracle/tomcat/3isc/logs
rm -rf *
~~~
## 启动
~~~
cd /home/oracle/tomcat/3isc/bin
chmod a+x *.sh

#后台启动 or
./catalina.sh start
#前台启动
./catalina.sh run
~~~

## 查看日志

~~~
tail -f /home/oracle/tomcat/3isc/logs/catalina.out
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/172142_3e1ecb73_602805.png "屏幕截图.png")

## 关闭
~~~
ps -ef|grep 3isc
kill -9 pid
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/172258_42c7b676_602805.png "屏幕截图.png")