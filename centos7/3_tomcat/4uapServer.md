# 4uapServer

## 准备
~~~
su - root

cd /home/oracle/tomcat

wget ftp://192.168.9.5/sgsms/centos/4uapServer.zip --ftp-user=nice --ftp-password=nice

unzip 4uapServer.zip
~~~

## 清空日志
~~~
cd /home/oracle/tomcat/4uapServer/logs
rm -rf *.log
~~~

## 启动
~~~
cd /home/oracle/tomcat/4uapServer/bin
chmod a+x *.sh

#后台启动 or
./catalina.sh start
#前台启动
./catalina.sh run
~~~

## 查看日志
~~~
tail -f /home/oracle/tomcat/4uapServer/logs/catalina.out
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/111452_cbd2b5c7_602805.png "屏幕截图.png")

## 关闭
~~~
ps -ef|grep 4uapServer
kill -9 pid
~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0727/173307_dac377db_602805.png "屏幕截图.png")

