## 下载jdk-7u80-linux-x64.tar.gz
~~~
百度云地址：
链接：https://pan.baidu.com/s/1geIDX1T 密码：7780

su - root

cd /root

rz
~~~

## 解压
~~~
tar zxvf jdk-7u80-linux-x64.tar.gz

~~~

## 切换java版本
~~~
alternatives --install /usr/bin/java java /root/jdk-7u80-linux-x64/bin/java 3
alternatives --config java
#输入3
~~~

## 配置环境变量
~~~
vi /etc/profile
#追加

export JAVA_HOME=/root/jdk-7u80-linux-x64
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:%JAVA_HOME/bin
~~~

## 激活配置文件
~~~
source /etc/profile
~~~

## 查看java版本
~~~
java -version
~~~



