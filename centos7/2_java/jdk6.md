## 下载jdk-6u6-linux-x64.bin
~~~
su - root

cd /root
wget ftp://192.168.9.5/sgsms/centos/jdk-6u6-linux-x64.bin --ftp-user=nice --ftp-password=nice
~~~

## 安装
~~~
sh jdk-6u6-linux-x64.bin
#一路回车
~~~

## 切换java版本
~~~
alternatives --install /usr/bin/java java /root/jdk1.6.0_06/bin/java 3
alternatives --config java
#输入3
~~~
## 查看java版本
~~~
java -version

~~~
![输入图片说明](https://git.oschina.net/uploads/images/2017/0731/220718_d3ee3fbd_602805.png "屏幕截图.png")

## 配置环境变量
~~~
vi /etc/profile
#追加

export JAVA_HOME=/root/jdk1.6.0_06
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:%JAVA_HOME/bin
~~~

## 激活配置文件
~~~
source /etc/profile
~~~



