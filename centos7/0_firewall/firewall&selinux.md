~~~
#添加 --permanent永久生效，没有此参数重启后失效
firewall-cmd --zone=public --add-port=3306/tcp --permanent
#重新载入
firewall-cmd --reload
#查看
firewall-cmd --zone= public --query-port=3306/tcp
#删除
firewall-cmd --zone= public --remove-port=3306/tcp --permanent
~~~



## 临时关闭selinux

setenforce 0

## 永久关闭selinux

vi /etc/selinux/config
![输入图片说明](https://git.oschina.net/uploads/images/2017/0724/133311_2e1bfefe_602805.png "屏幕截图.png")

## 停止firewalld 

systemctl stop firewalld



# 忽略以下内容（仅供参考）

# selinux

## 查看sestatus

![输入图片说明](https://git.oschina.net/uploads/images/2017/0724/132920_c999e516_602805.png "屏幕截图.png")


## 临时关闭

setenforce 0


## 临时开启

setenforce 1


## 永久关闭
vi /etc/selinux/config
![输入图片说明](https://git.oschina.net/uploads/images/2017/0724/133311_2e1bfefe_602805.png "屏幕截图.png")

#firewall

## 启动

systemctl start firewalld

## 停止 

systemctl stop firewalld

## help

firewall-cmd --help |grep port

## 开放端口

firewall-cmd --zone=public --add-port=1521/tcp --permanent

## 重启

firewall-cmd --reload

## 查看端口状态

firewall-cmd --zone=public --query-port=1521/tcp --permanent

## 删除端口

firewall-cmd --zone=public --remove-port=1521/tcp --permanent