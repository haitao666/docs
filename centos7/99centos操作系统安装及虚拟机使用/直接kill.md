ps -ef |grep tomcat |awk '{print $2}'|xargs kill -9

ps -ef |grep java |awk '{print $2}'|xargs kill -9

ps -ef |grep 1mq |awk '{print $2}'|xargs kill -9

ps -ef |grep 2sso |awk '{print $2}'|xargs kill -9

ps -ef |grep 3isc |awk '{print $2}'|xargs kill -9

ps -ef |grep 4uap |awk '{print $2}'|xargs kill -9

ps -ef |grep 5sgsms |awk '{print $2}'|xargs kill -9