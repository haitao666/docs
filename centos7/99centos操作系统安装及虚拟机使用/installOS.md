# 下载

VirtualBox：https://www.virtualbox.org/wiki/Downloads

Centos7：https://www.centos.org/download/

公司ftp：192.168.9.5/sgsms/centos/CentOS-7-x86_64-Everything-1611.iso
ftp用户名密码：nice/nice

# 安装centos7

新建虚拟机并添加操作系统镜像

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-2a60e909aa9d2fb7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-cb20573ac7b0195c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-f73b58a45783c199.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-c709ba74c4f817db.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-f16299960575576d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-250c0c11232142b8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-5476f51a4b490ea8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

##启动虚拟机，光盘启动进行操作系统安装

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-74a1422b53b08e07.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-328a03c05fae7b70.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-0d2bc2f645f278c2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

![输入图片说明](http://upload-images.jianshu.io/upload_images/2907900-062a0817429bbb58.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "在这里输入图片标题")

## NAT方式连接虚拟机
### 查看虚拟机ip

![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/105644_dd40c679_602805.png "屏幕截图.png")
### 设置主机到虚拟机的端口转发
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/105807_c27fa1f9_602805.png "屏幕截图.png")

### ssh连接（连接主机的ip端口即可）
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/105937_619b890d_602805.png "屏幕截图.png")

 **！如果发现ssh登陆很慢** 
root 进入/etc/ssh/目录，然后vi sshd_config，
/UseDNS yes 查找到这一行，然后i进入编辑模式，改成UseDNS no，前面的注释（#）去掉。
![输入图片说明](https://gitee.com/uploads/images/2017/1109/154237_f3a155e5_950371.png "屏幕截图.png")
重启ssh服务  /etc/init.d/sshd restart







