# CentOS

## 备份

mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup

## 下载新的CentOS-Base.repo 到/etc/yum.repos.d/


### 网易yum源
~~~
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.163.com/.help/CentOS7-Base-163.repo

或

curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.163.com/.help/CentOS7-Base-163.repo
~~~

### 阿里云yum源

~~~
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

或

curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
~~~

## epel源
~~~
yum -y install epel-release
~~~

## 生成缓存
yum makecache

## yum update -y
升级所有包，改变软件设置和系统设置,系统版本内核都升级


