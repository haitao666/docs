# 下载gradle4.2并解压
外网下载链接：https://services.gradle.org/distributions/gradle-4.2-bin.zip
![输入图片说明](https://gitee.com/uploads/images/2017/1113/192248_abe8317a_602805.png "屏幕截图.png")

# 将gradle加入系统环境变量
![输入图片说明](https://gitee.com/uploads/images/2017/1113/192434_86784fd8_602805.png "屏幕截图.png")

![输入图片说明](https://gitee.com/uploads/images/2017/1113/192604_c428e50d_602805.png "屏幕截图.png")

# cmd窗口查看gradle版本
![输入图片说明](https://gitee.com/uploads/images/2017/1113/192709_a807d86d_602805.png "屏幕截图.png")

# linux服务器上使用gradle打包（非教程示例）
> 参考：https://gradle.org/install/
![输入图片说明](https://gitee.com/uploads/images/2017/1113/195616_d8d26ef6_602805.png "屏幕截图.png")