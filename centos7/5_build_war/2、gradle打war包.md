# 下载最新代码
![输入图片说明](https://gitee.com/uploads/images/2017/1113/184312_026f5c75_602805.png "屏幕截图.png")

# 修改sgsms模块的gradle.properties文件（仅需初次打包时配置）
![输入图片说明](https://gitee.com/uploads/images/2017/1113/185924_19caaac8_602805.png "屏幕截图.png")

# cmd命令窗口，进入sgsms目录下
执行gradle clean
![输入图片说明](https://images.gitee.com/uploads/images/2019/0514/170854_8b504d32_602805.png "屏幕截图.png")

执行gradle build
![输入图片说明](https://images.gitee.com/uploads/images/2019/0514/170926_932329fd_602805.png "屏幕截图.png")

# 打包成功截图
![输入图片说明](https://gitee.com/uploads/images/2017/1113/193228_30cb0957_602805.png "屏幕截图.png")

# war包目录

![输入图片说明](https://gitee.com/uploads/images/2017/1113/193703_b29c35a0_602805.png "屏幕截图.png")

# 常见错误处理
## 检查模块下build.gradle的依赖是否与MANIFEST.MF一致
![输入图片说明](https://images.gitee.com/uploads/images/2019/0514/171933_8dab783b_602805.png "屏幕截图.png")

