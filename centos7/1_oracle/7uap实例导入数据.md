## oracle用户创建数据库备份目录

~~~
su - oracle

mkdir -p /home/oracle/app/oracle/oradata_bak/uap
mkdir -p /home/oracle/app/oracle/oradata/uap

cd /home/oracle/app/oracle/oradata_bak/uap

wget ftp://192.168.9.5/sgsms/centos/20170713.uap.DMP --ftp-user=nice --ftp-password=nice
~~~

##以system用户连接本地uap实例（地址以实际为准）

![输入图片说明](https://git.oschina.net/uploads/images/2017/0725/163033_64e19765_602805.png "屏幕截图.png")

![输入图片说明](https://gitee.com/uploads/images/2017/1102/105546_86e456a3_602805.png "屏幕截图.png")

## 进入命令窗口
![输入图片说明](https://git.oschina.net/uploads/images/2017/0725/163128_9ed3d4f9_602805.png "屏幕截图.png")

## 命令窗口执行sql（drop时错误可以忽略）
~~~
CREATE TABLESPACE "TS_UAP"
    DATAFILE '/home/oracle/app/oracle/oradata/uap/TS_UAP.ORA'
    SIZE 20M
AUTOEXTEND ON
NEXT 32M MAXSIZE 2048M
EXTENT MANAGEMENT LOCAL;

drop user uap cascade;
CREATE USER uap IDENTIFIED BY uap DEFAULT TABLESPACE TS_UAP;
GRANT DBA TO uap;
~~~

## 创建数据库服务端目录（路径以实际为准）
~~~
create or replace directory bakup_dir_uap as '/home/oracle/app/oracle/oradata_bak/uap';
~~~

## cmd窗口中导入备份的数据文件（ip、路径以实际为准）
~~~
impdp uap/uap@127.0.0.1:1521/uap directory=bakup_dir_uap dumpfile=20170713.uap.DMP full=y 
~~~

> 其他方式导入的impdp(仅做参考)

~~~
impdp uap/uap@127.0.0.1:1521/orcl directory=bakup_dir_uap dumpfile=20170713.uap.DMP schemas=uap
impdp uap/uap@127.0.0.1:1521/orcl directory=bakup_dir_uap dumpfile=20170713.uap.DMP REMAP_SCHEMA=userOld:userNew
~~~