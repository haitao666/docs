# 添加oracle用户

~~~
groupadd oracle

useradd -g oracle -m oracle

passwd oracle
~~~


# 下载安装文件到目录（/home/oracle/install）

~~~
su - oracle

mkdir -p /home/oracle/install

cd /home/oracle/install

wget ftp://192.168.9.5/sgsms/centos/compat-libcap1-1.10-7.el7.x86_64.rpm --ftp-user=nice --ftp-password=nice

wget ftp://192.168.9.5/sgsms/centos/linux.x64_11gR2_database_1of2.zip --ftp-user=nice --ftp-password=nice

wget ftp://192.168.9.5/sgsms/centos/linux.x64_11gR2_database_2of2.zip --ftp-user=nice --ftp-password=nice

unzip linux.x64_11gR2_database_1of2.zip

unzip linux.x64_11gR2_database_2of2.zip
~~~