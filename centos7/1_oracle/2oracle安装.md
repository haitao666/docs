以oracle登录图形化的centos7

# 安装oracle
cd /home/oracle/install/database

export LANG=en

./runInstaller

![输入图片说明1](https://git.oschina.net/uploads/images/2017/0724/144012_716cade0_602805.png "屏幕截图.png1")

![输入图片说明2](https://git.oschina.net/uploads/images/2017/0724/144503_71996dd5_602805.png "屏幕截图.png2")

![输入图片说明3](https://git.oschina.net/uploads/images/2017/0724/144411_f0a43474_602805.png "屏幕截图.png3")

![输入图片说明4](https://git.oschina.net/uploads/images/2017/0724/144542_85965df6_602805.png "屏幕截图.png4")

![输入图片说明5](https://git.oschina.net/uploads/images/2017/0724/144614_f257cabb_602805.png "屏幕截图.png5")

![输入图片说明6](https://git.oschina.net/uploads/images/2017/0724/144653_821e5bb3_602805.png "屏幕截图.png6")

![输入图片说明7](https://git.oschina.net/uploads/images/2017/0724/144737_040b6654_602805.png "屏幕截图.png7")

![输入图片说明8](https://git.oschina.net/uploads/images/2017/0724/144814_64619793_602805.png "屏幕截图.png8")

![输入图片说明9](https://git.oschina.net/uploads/images/2017/0724/144854_e1a770e4_602805.png "屏幕截图.png9")

![输入图片说明10](https://git.oschina.net/uploads/images/2017/0724/145302_4abfce50_602805.png "屏幕截图.png10")

![输入图片说明11](https://git.oschina.net/uploads/images/2017/0724/150436_1ea5dfbb_602805.png "屏幕截图.png11")

# 设置环境变量

vi /home/oracle/.bash_profile
~~~
# .bash_profile
# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH
export ORACLE_BASE=/home/oracle/app/oracle
export ORACLE_HOME=$ORACLE_BASE/product/11.2.0/dbhome_1
export PATH=$ORACLE_HOME/bin:/usr/sbin:$PATH
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib
export LANG="en_US.UTF-8"
export NLS_LANG="SIMPLIFIED CHINESE_CHINA.ZHS16GBK"
~~~

## 使环境变量永久生效

注销oracle用户并重新以oracle登录

## root用户执行使环境变量立即生效

source /home/oracle/.bash_profile


























