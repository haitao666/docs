## oracle用户创建数据库备份目录

~~~
su - oracle

mkdir -p /home/oracle/app/oracle/oradata_bak/orcl

cd /home/oracle/app/oracle/oradata_bak/orcl

wget ftp://192.168.9.5/sgsms/centos/20170713.orcl.DMP --ftp-user=nice --ftp-password=nice
~~~

## 以system用户连接本地orcl实例（地址以实际为准）

![输入图片说明](https://git.oschina.net/uploads/images/2017/0725/163033_64e19765_602805.png "屏幕截图.png")

![输入图片说明](https://git.oschina.net/uploads/images/2017/0725/163039_bacbc53a_602805.png "屏幕截图.png")

## 进入命令窗口
![输入图片说明](https://git.oschina.net/uploads/images/2017/0725/163128_9ed3d4f9_602805.png "屏幕截图.png")

## 命令窗口执行sql（drop时错误可以忽略）
~~~
CREATE TABLESPACE "TS_ORCL"
    DATAFILE 'D:\APP\WEB\ORADATA\UAP\TS_ORCL.ORA'
    SIZE 20M
AUTOEXTEND ON
NEXT 32M MAXSIZE 2048M
EXTENT MANAGEMENT LOCAL;


drop user iscv2106 cascade;
CREATE USER iscv2106 IDENTIFIED BY iscv2106 DEFAULT TABLESPACE TS_ORCL;
GRANT DBA TO iscv2106;
~~~

## 创建数据库服务端目录（路径以实际为准）
~~~
create or replace directory bakup_dir_orcl as '/home/oracle/app/oracle/oradata_bak/orcl';
~~~

## cmd窗口中导入备份的数据文件（ip、路径以实际为准）
~~~
impdp iscv2106/iscv2106@127.0.0.1:1521/orcl directory=bakup_dir_orcl dumpfile=20170713.orcl.DMP full=y 
~~~

> 其他方式导入的impdp(仅做参考)

~~~
impdp iscv2106/iscv2106@127.0.0.1:1521/orcl directory=bakup_dir_orcl dumpfile=20170713.orcl.DMP schemas=iscv2106
impdp iscv2106/iscv2106@127.0.0.1:1521/orcl directory=bakup_dir_orcl dumpfile=20170713.orcl.DMP REMAP_SCHEMA=userOld:userNew
~~~
