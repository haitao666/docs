## 安装oracle依赖包

root用户执行
~~~
su - root

yum install -y binutils

rpm -ivh /home/oracle/install/compat-libcap1-1.10-7.el7.x86_64.rpm 

yum install -y compat-libstdc++-33

yum install -y gcc

yum install -y glibc

yum install -y glibc-devel

yum install -y ksh

yum install -y libgcc

yum install -y libstdc++

yum install -y libstdc++-devel

yum install -y libaio

yum install -y libaio-devel

yum install -y make

yum install -y sysstat
~~~

## 修改OS版本信息
~~~
vi /etc/redhat-release
cat /etc/redhat-release

#CentOS Linux release 7.3.1611 (Core)
Red Hat Enterprise Linux 7
~~~