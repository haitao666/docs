# Linux下oracle数据库启动和关闭操作
## 监听 
~~~
lsnrctl start
~~~
~~~
export ORACLE_SID=orcl
su oracle

sqlplus /nolog

connect /as sysdba

startup 
startup mount

~~~

不带参数，启动数据库实例并打开数据库，以便用户使用数据库，在多数情况下，使用这种方式！ 

mount，在进行数据库修改配置时使用

nomount，只启动数据库实例，但不打开数据库，在你希望创建一个新的数据库时使用，或者在你需要这样的时候使用！ 

## shutdown
~~~
shutdown 
shutdown immediate
shutdown transactional 
shutdown abort 
~~~
normal 需要等待所有的用户断开连接 

normal 需要在所有连接用户断开后才执行关闭数据库任务，所以有的时候看起来好象命令没有运行一样！在执行这个命令后不允许新的连接 

immediate 等待用户完成当前的语句 

immediate 在用户执行完正在执行的语句后就断开用户连接，并不允许新用户连接。

transactional 等待用户完成当前的事务 

transactional 在拥护执行完当前事物后断开连接，并不允许新的用户连接数据库。 

abort 不做任何等待，直接关闭数据库 

abort 执行强行断开连接并直接关闭数据库。 

