# 安装vsftpd

~~~
su - root

yum -y install vsftpd
~~~

# 设置ftp

~~~
sed -i "s/anonymous_enable=YES/anonymous_enable=NO/g" '/etc/vsftpd/vsftpd.conf'
sed -i "s/#chroot_local_user=YES/chroot_local_user=YES/g" '/etc/vsftpd/vsftpd.conf'
echo -e "allow_writeable_chroot=YES" >> /etc/vsftpd/vsftpd.conf
~~~

# 添加ftp共享目录

~~~
mkdir -p /ftp/sgsms/fileStore
~~~

# 添加ftp用户

~~~
useradd -s /sbin/nologin -d /ftp uap
#密码uap（与sgsms项目代码保持一致）
passwd uap
~~~

# 设置ftp用户权限

~~~
chown -R uap /ftp
~~~

# 启动ftp

~~~
systemctl start vsftpd
~~~
