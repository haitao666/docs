# centos7

> CentOS 7 版本，由于 MySQL数据库已从默认的程序列表中移除，可以使用 mariadb 代替

~~~
yum install mariadb-server mariadb -y

#启动MariaDB
systemctl start mariadb
#停止MariaDB
systemctl stop mariadb
#重启MariaDB
systemctl restart mariadb
#设置开机启动
systemctl enable mariadb

mysqladmin --version


mysql -u root -p

~~~