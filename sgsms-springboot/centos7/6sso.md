# 初始化数据库

https://gitee.com/spnice00/fork-smart/blob/master/smart-sso/smart-sso.sql

![输入图片说明](https://gitee.com/uploads/images/2018/0330/133546_3d972ad2_602805.png "屏幕截图.png")

# 时钟同步
~~~
yum -y install ntp
chkconfig ntpd on
ntpdate time.apple.com
~~~

~~~
yum install git -y
~~~

~~~
cd /root/sgsms-springboot/
git clone https://gitee.com/spnice00/sgsms-springboot.git
cd sgsms-springboot
mvn clean install
~~~

~~~
cd /root/sgsms-springboot/
git clone https://gitee.com/spnice00/fork-smart.git
cd /root/sgsms-springboot/fork-smart/
mvn clean install -P dev
~~~

~~~
cd /root/sgsms-springboot/fork-smart/smart-static/

# 后台启动，查看日志nohup.out
nohup mvn clean jetty:run &
tail -f nohup.out

或者

# 后台启动，查看日志sso-static.log
nohup mvn clean jetty:run > sso-static.log 2>&1 &
tail -f sso-static.log

~~~
![输入图片说明](https://gitee.com/uploads/images/2018/0330/141800_c98f4b7d_602805.png "屏幕截图.png")

~~~
cd /root/sgsms-springboot/fork-smart/smart-sso/smart-sso-server/

# 后台启动，查看日志nohup.out
nohup mvn clean jetty:run -Djava.awt.headless=true &
tail -f nohup.out

或者

# 后台启动，查看日志sso-static.log
nohup mvn clean jetty:run -Djava.awt.headless=true > sso-static.log 2>&1 &
tail -f sso-static.log

~~~