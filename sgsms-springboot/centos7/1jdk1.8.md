# 下载安装
~~~
mkdir sgsms-springboot

cd sgsms-springboot/

# 上传jdk、maven
rz

tar zxvf jdk-8u162-linux-x64.tar.gz

alternatives --install /usr/bin/java java /root/sgsms-springboot/jdk1.8.0_162/bin/java 3

alternatives --config java

~~~

# 配置环境变量

~~~
vi /etc/profile
#追加

export JAVA_HOME=/root/sgsms-springboot/jdk1.8.0_162
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:%JAVA_HOME/bin

~~~

# 激活配置文件

~~~
source /etc/profile
~~~

查看java版本

~~~
java -version
~~~

![输入图片说明](https://gitee.com/uploads/images/2018/0330/095917_01d4ff9d_602805.png "屏幕截图.png")