# 安装
~~~
cd /root/sgsms-springboot/

# 上传apache-maven-3.5.3-bin.tar.gz
rz

tar xzvf apache-maven-3.5.3-bin.tar.gz 

cd apache-maven-3.5.3/

vi /etc/profile
#追加

export PATH=/root/sgsms-springboot/apache-maven-3.5.3/bin:$PATH
~~~


# 激活配置文件

~~~

source /etc/profile

~~~

# 查看maven版本

~~~

mvn -v

~~~
![输入图片说明](https://gitee.com/uploads/images/2018/0330/102225_279f09a9_602805.png "屏幕截图.png")