~~~
mkdir /usr/local/activemq

cd /usr/local/activemq

# 上传apache-activemq-5.13.3-bin.tar.gz
rz

tar -xzf apache-activemq-5.13.3-bin.tar.gz

rm -rf apache-activemq-5.13.3-bin.tar.gz

cd /etc/init.d/
~~~


vim activemq
~~~
#!/bin/sh
export JAVA_HOME=/usr/jdk8
export CATALINA_HOME=/usr/local/activemq/apache-activemq-5.13.3
case $1 in
    start)
        sh $CATALINA_HOME/bin/activemq start
    ;;
    stop)
        sh $CATALINA_HOME/bin/activemq stop
    ;;
    restart)
        sh $CATALINA_HOME/bin/activemq stop
        sleep 1
        sh $CATALINA_HOME/bin/activemq start
    ;;
esac
exit 0
~~~


~~~
chmod 777 activemq
chkconfig activemq on
service activemq start
访问： 
http://sso.com:8161 
默认用户名密码为：admin/admin
~~~