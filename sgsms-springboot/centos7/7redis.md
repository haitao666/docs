下载
~~~
cd /opt
wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war
~~~

启动jenkins
~~~
java -jar jenkins.war --httpPort=8081
~~~

配置jenkins
~~~
http://sso.com:8081
user：admin
pwd：在/root/.jenkins/secrets/initialAdminPassword文件里（其中.jenkins位置以启动war时日志里提示的为准）


#添加 --permanent永久生效，没有此参数重启后失效
firewall-cmd --zone=public --add-port=8081/tcp --permanent
#重新载入
firewall-cmd --reload

~~~
![输入图片说明](https://gitee.com/uploads/images/2018/0125/112025_b67301de_602805.png "屏幕截图.png")