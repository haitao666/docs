# 配置可以远程管理tomcat的ip
~~~
vim tomcat8.5.29-8082/webapps/manager/META-INF/context.xml
~~~
![输入图片说明](https://gitee.com/uploads/images/2018/0408/183104_75eef135_602805.png "屏幕截图.png")

# 设置管理tomcat的密码
~~~
vim tomcat8.5.29-8082/conf/tomcat-users.xml

<role rolename="manager-gui"/>
<role rolename="manager-script"/>
<role rolename="manager-jmx"/>
<role rolename="manager-status"/>
<user username="admin" password="admin" roles="manager-gui,manager-script,manager-jmx,manager-status"/>
~~~
![输入图片说明](https://gitee.com/uploads/images/2018/0408/183308_f8496515_602805.png "屏幕截图.png")