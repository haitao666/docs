# mysql-community-common-5.7.21-1.el7.x86_64

## 下载安装
~~~
mkdir /root/sgsms-springboot/mysql

cd /root/sgsms-springboot/mysql

# 下载
https://dev.mysql.com/downloads/mysql/

# 这里rz直接上传已下载的文件
rz

# 删除冲突的包（mariadb）
rpm -e postfix-2:2.10.1-6.el7.x86_64
rpm -e mariadb-libs-1:5.5.52-1.el7.x86_64

# 安装
rpm -ivh mysql-community-common-5.7.21-1.el7.x86_64.rpm
rpm -ivh mysql-community-libs-5.7.21-1.el7.x86_64.rpm
rpm -ivh mysql-community-libs-compat-5.7.21-1.el7.x86_64.rpm
rpm -ivh mysql-community-client-5.7.21-1.el7.x86_64.rpm
rpm -ivh mysql-community-server-5.7.21-1.el7.x86_64.rpm

mysqladmin --version
~~~

## 设置root密码
 
~~~
vim /etc/my.cnf
添加
skip-grant-tables
lower_case_table_names=1
~~~
![输入图片说明](https://gitee.com/uploads/images/2018/0330/115513_dbad92fe_602805.png "屏幕截图.png")
~~~
systemctl restart mysqld
mysql -u root
show databases;
use mysql;
flush privileges;
set password for 'root'@'localhost' = password('root');
exit;

vim /etc/my.cnf
注释掉下行
#skip-grant-tables

systemctl restart mysqld
~~~


## 创建数据库实例、用户、赋权

~~~
mysql -u root -p
create database sso;
CREATE USER 'sso'@'%' IDENTIFIED BY "SSOsso@123123";
GRANT all ON *.* TO sso@'%';

create database sms;
CREATE USER 'sms'@'%' IDENTIFIED BY "SMSsms@123123";
GRANT all ON *.* TO sms@'%';
~~~

防火墙
~~~
#添加（--permanent永久生效，没有此参数重启后失效）
firewall-cmd --zone=public --add-port=3306/tcp --permanent
#重新载入
firewall-cmd --reload
#查看
firewall-cmd --zone= public --query-port=3306/tcp
#删除
firewall-cmd --zone= public --remove-port=3306/tcp --permanent
~~~

# 其他示例

~~~
#停外键
set foreign_key_checks=0;
#启用外键
set foreign_key_checks=1;

#启动
systemctl start mysqld
#停止
systemctl stop mysqld
#重启
systemctl restart mysqld
#设置开机启动
systemctl enable mysqld
~~~