# 1解压jdk
![输入图片说明](https://images.gitee.com/uploads/images/2018/0910/095210_fccabf96_602805.png "屏幕截图.png")
# 2环境变量
![输入图片说明](https://images.gitee.com/uploads/images/2018/0910/095217_ec577bd9_602805.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0910/095227_df19a183_602805.png "屏幕截图.png")
# 3JAVA_HOME
![输入图片说明](https://images.gitee.com/uploads/images/2018/0910/095232_620bd23e_602805.png "屏幕截图.png")
# 4PATH
%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin;
![输入图片说明](https://images.gitee.com/uploads/images/2018/0910/095237_143169b7_602805.png "屏幕截图.png")
# 5CLASSPATH
.;%JAVA_HOME%\lib;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar
![输入图片说明](https://images.gitee.com/uploads/images/2018/0910/095243_eab10b71_602805.png "屏幕截图.png")