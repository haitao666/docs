# 初始化sso数据库

1. 打开命令提示符

2. 连接到mysql数据库  
~~~
cd C:\work\soft\mysql-5.7.21-winx64\bin  
mysql -u root -p
~~~

3. 输入密码  
SSOsso@123123

4. 导入sql文件  
~~~
use sso
source C:\Users\Administrator\Desktop\bak\mysql-sso\sso_ansi.sql
~~~

![输入图片说明](https://images.gitee.com/uploads/images/2018/0911/114243_8090fdaf_903874.png "屏幕截图.png")

如图所示即为导入成功