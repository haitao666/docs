# 下载
链接：https://pan.baidu.com/s/1aeDtFpeWX_Y3eUpjjuWRpw 密码：yx98

# 安装vcredist_x64.exe

# 解压mysql-5.7.21-winx64.zip

# 在mysql-5.7.21主目录创建my.ini文件
~~~
[mysql]
# 设置mysql客户端默认字符集
default-character-set=utf8 
[mysqld]
#设置3306端口
port = 3306 
# 设置mysql的安装目录
basedir=D:\\softnew\\MYSQL\\mysql-5.7.20-winx64
# 允许最大连接数
max_connections=200
# 服务端使用的字符集默认为8比特编码的latin1字符集
character-set-server=utf8
# 创建新表时将使用的默认存储引擎
default-storage-engine=INNODB
~~~

# 进入mysql解压后的bin目录，以管理员运行cmd
~~~
mysqld --initialize
mysqld install
net start mysql
~~~

# 复制root密码
![输入图片说明](https://images.gitee.com/uploads/images/2018/0910/104551_86c1e7ee_602805.png "屏幕截图.png")

# 使用复制的密码连接mysql
![输入图片说明](https://images.gitee.com/uploads/images/2018/0910/104636_d8a239c5_602805.png "屏幕截图.png")

# 创建sso用户
~~~
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';
create database sso;
CREATE USER 'sso'@'%' IDENTIFIED BY "SSOsso@123123";
GRANT all ON *.* TO sso@'%';
~~~

# 下载mysql客户端，并使用sso用户登录，测试
链接：https://pan.baidu.com/s/11-80ikgHUxbHd0M6RCIc2w 密码：ytp8
