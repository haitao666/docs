# 1 choco
 _choco 是windows下的包管理工具_

 _choco 默认的安装目录为：C:\ProgramData_

## 1.1 安装choco
 _cmd窗口运行_
~~~
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
~~~

# 2 nvm
 _nvm是管理node版本的工具_

## 2.1 安装nvm
~~~
choco install nvm
~~~
 _添加国内镜像，修改C:\ProgramData\nvm\settings.txt,添加下面两行_
~~~
node_mirror: https://npm.taobao.org/mirrors/node/
npm_mirror: https://npm.taobao.org/mirrors/npm/
~~~

# 3 安装node
~~~
##查询可用node版本
nvm list available
##安装指定node版本
nvm install 10.15.2
##使用指定node版本
nvm use 10.15.2
##测试
node -v
npm -v
~~~