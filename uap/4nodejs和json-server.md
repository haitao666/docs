# 1 nodejs

## 1.1 解压node-v8.9.4-win-x64.zip

## 1.2 设置环境变量
![输入图片说明](https://gitee.com/uploads/images/2018/0205/094049_c87f5c3a_602805.png "屏幕截图.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0205/094135_08317038_602805.png "屏幕截图.png")

# 2 json-server.js
[输入链接说明](https://github.com/typicode/json-server)
~~~
npm install -g json-server
~~~

# 3 faker.js

[输入链接说明](https://github.com/Marak/faker.js)

# 4 新建index.js
~~~
var faker = require('faker');
faker.locale = "zh_CN";
module.exports = () => {
  //data 为api接口测试数据
  const data = { users: [] }
  // Create 1000 users
  for (let i = 0; i < 1000; i++) {
    //faker随机生成测试数据
    data.users.push({ id: 'id'+i, name: faker.name.findName() })
  }
  return data
}
~~~
# 5 启动
~~~
json-server index.js
~~~

![输入图片说明](https://gitee.com/uploads/images/2018/0205/095037_bfcf7dfe_602805.png "屏幕截图.png")

# 6 测试api接口
~~~
http://127.0.0.1:3000/users
http://127.0.0.1:3000/users?name_like=王
http://127.0.0.1:3000/users?name_like=王&_page=1&_limit=10
http://127.0.0.1:3000/users?name_like=王&_page=1&_limit=10&_sort=name&_order=asc
~~~
//更多参考
[输入链接说明](https://github.com/typicode/json-server)