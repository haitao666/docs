##客户机文件系统编码

~~~
一般
windows客户机文件系统编码 : gbk
linux客户机文件系统编码 : utf-8
~~~

![输入图片说明](https://git.oschina.net/uploads/images/2017/0817/174124_e412014c_602805.png "屏幕截图.png")

##浏览器页面编码

~~~
一般为utf-8
~~~

![输入图片说明](https://git.oschina.net/uploads/images/2017/0817/173817_db61327b_602805.png "屏幕截图.png")

##jvm编码

~~~
一般默认为jvm所在操作系统编码
~~~

![输入图片说明](https://git.oschina.net/uploads/images/2017/0817/173854_0447a4f5_602805.png "屏幕截图.png")

##tomcat编码

~~~
tomcat6以前默认为iso-8859-1，可以修改
~~~

![输入图片说明](https://git.oschina.net/uploads/images/2017/0817/174016_6abf8a69_602805.png "屏幕截图.png")

###过滤器统一设置编码

~~~
ServletRequest的ServletRequest方法
~~~

![![输入图片说明](https://git.oschina.net/uploads/images/2017/0817/174839_75ee0a85_602805.png "屏幕截图.png")](https://git.oschina.net/uploads/images/2017/0817/174838_10e4d213_602805.png "屏幕截图.png")

##ftp服务端编码

~~~
windows搭建FileZilla Server：默认为iso-8859-1
linux搭建vsftpd Server：默认为utf-8
~~~

![输入图片说明](https://git.oschina.net/uploads/images/2017/0817/174313_6209a3a4_602805.png "屏幕截图.png")

##数据库编码

![输入图片说明](https://git.oschina.net/uploads/images/2017/0817/174205_8315ae37_602805.png "屏幕截图.png")