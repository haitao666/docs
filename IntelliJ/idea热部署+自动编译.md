# 效果
在鼠标移除idea窗口或者执行编译后，tomcat自动热部署
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/111659_b35e1cf5_602805.png "屏幕截图.png")
## idea热部署
~~~
on update action
    触发更新的操作，如点击编译按钮
on frame deactivation
    idea窗口时区焦点，如离开idea窗口，切换到浏览器或者系统桌面
update class ： 更新class操作
update resources： 更新xml、property等资源文件
~~~
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/110250_2c00852d_602805.png "屏幕截图.png")

## 设置自动编译
~~~
ctrl+alt+s 设置界面开启自动编译（注意：only works while not running/debugging）
~~~
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/110357_e58b0570_602805.png "屏幕截图.png")

## 设置running/debugging时也能自动编译
~~~
ctrl+shift+a
~~~
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/110640_f6ea01a1_602805.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/110706_73681a4d_602805.png "屏幕截图.png")