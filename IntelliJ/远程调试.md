# 服务器端启动时设置允许远程调试
~~~
java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 -jar demo-0.0.1-SNAPSHOT.jar
~~~

# idea里新建启动配置
![输入图片说明](https://images.gitee.com/uploads/images/2018/1106/153021_0947fce4_602805.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1106/153105_7dc086fe_602805.png "屏幕截图.png")

# idea调试启动
![输入图片说明](https://images.gitee.com/uploads/images/2018/1106/153208_eb406470_602805.png "屏幕截图.png")