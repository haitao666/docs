
# idea Project Structure 讲解

> 原文：https://www.cnblogs.com/zadomn0920/p/6196962.html
 

## 项目的左侧面板
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120044_e481ebc9_602805.png "屏幕截图.png")

## 项目设置->Project
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120054_dfafd3fa_602805.png "屏幕截图.png")


## Project Settings -> Modules
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120102_d5956619_602805.png "屏幕截图.png")

## Sources面板
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120113_4bb04a13_602805.png "屏幕截图.png")

## Paths面板
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120123_5e54d5cd_602805.png "屏幕截图.png")

## dependencies面板
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120127_f5c924b8_602805.png "屏幕截图.png")

 

 

 ## Project Settings - > Libraries

![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120132_337e71b4_602805.png "屏幕截图.png")
 

 ## Project Settings - > Facets
 
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120137_2a1876e3_602805.png "屏幕截图.png")
 

## Project Settings -> artifacts

![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120142_c69a7324_602805.png "屏幕截图.png")
 

 

## 关于添加依赖包
可以在Libraries标签页中增加架包，也可以在对应的module里添加架包。前者可以将架包集中在一起并起个别名，便于自己对架包的管理。

 ![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120148_b1a20dac_602805.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/120154_5251deb4_602805.png "屏幕截图.png")
